package com.fisa.curso.soap;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import com.fisa.curso.modelo.Persona;
import com.fisa.curso.negocio.PersonaService;

@WebService(endpointInterface = "com.fisa.curso.soap.PersonasResource", 
				  serviceName = "PersonasResource")
public class PersonasResourceBean implements PersonasResource{
	@Inject
	PersonaService personaService;	

	@Override
	@WebMethod
	public List<Persona> findPersonas(String identificacion) {
		List<Persona> filtradas = new LinkedList<>();
		try {
			List<Persona> personas = personaService.retrieve();
			for(Persona persona : personas){
				if(persona.getIdentificacion().startsWith(identificacion)){
					filtradas.add(persona);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return filtradas;
	}

}
