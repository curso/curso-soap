package com.fisa.curso.soap;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.fisa.curso.modelo.Persona;

@WebService
public interface PersonasResource {
	
	List<Persona> findPersonas(@WebParam(name="identificacion") String identificacion);
}
