package com.fisa.curso;

import java.net.URL;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

import com.fisa.curso.modelo.Persona;
import com.fisa.curso.soap.PersonasResource;

public class PersonasResourceTester {
	private static final QName SERVICE_NAME = new QName(
			"http://soap.curso.fisa.com/", "PersonasResource");
	private static final QName PORT_NAME = new QName("http://soap.curso.fisa.com/",
			"PersonasResourceBeanPort");

	private PersonasResourceTester() {
	}

	public static void main(String args[]) throws Exception {
		URL wsdl = new URL("http://mars:8080/curso-soap/PersonasResource?wsdl");
		Service service = Service.create(wsdl, SERVICE_NAME);
		// Endpoint Address
		String endpointAddress = "http://localhost:8080/curso-soap";
		// If web service deployed on Tomcat (either standalone or embedded)
		// as described in sample README, endpoint should be changed to:
		// String endpointAddress =
		// "http://localhost:8080/java_first_jaxws/services/hello_world";

		// Add a port to the Service
		service.addPort(PORT_NAME, SOAPBinding.SOAP11HTTP_BINDING,
				endpointAddress);

		PersonasResource hw = service.getPort(PersonasResource.class);
		List<Persona> personas = hw.findPersonas("11");
		for(Persona p : personas){
			System.out.println(p);
		}
	}

}
